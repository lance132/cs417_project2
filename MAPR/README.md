SEE Lessons.md for the portions if the project that work and what we have learned.

To compile our code go to our src folder.

Next run 

```javac com/google/protobuf/*.java```

Next run 

```javac hdfs/com/google/protobuf/*.java```

Next run 

```javac hdfs/*.java```

Next run 

```javac ds/hdfs/com/google/protobuf/*.java```

Next run 

```javac ds/hdfs/hdfs/com/google/protobuf/*.java```

Next run 

```javac ds/hdfs/hdfs/*.java```

Finally run 

```javac ds/hdfs/*.java```

Make sure to switch the ip and ports in the dn_config.txt and nn_config.txt for your test.

dn_config.txt and nn_config.txt are formatted as <name;ip;port>

Now that all of the code has been compiled to run each program:

Got to the src folder again and run

For the name node
```java ds.hdfs.NameNode```

For the data node
```java ds.hdfs.DataNode```

For the client
```java ds.hdfs.Client```