package ds.hdfs;

import java.io.*;
import java.util.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;
import com.google.protobuf.*;
import java.util.concurrent.TimeUnit;

public class Client
{
    //Variables Required
    public INameNode NNStub; //Name Node stub
    public IDataNode DNStub; //Data Node stub

    public Client() throws Exception
    {
        //Get the Name Node Stub
        //nn_details contain NN details in the format Server;IP;Port

	BufferedReader br = new BufferedReader(new FileReader("nn_config.txt"));
	String line = br.readLine();
	String[] details = line.split(";");
	NNStub = GetNNStub(details[0], details[1], Integer.parseInt(details[2]));

	br = new BufferedReader(new FileReader("dn_config.txt"));
	line = br.readLine();
	details = line.split(";");
	DNStub = GetDNStub(details[0], details[1], Integer.parseInt(details[2]));

	br.close();
    }

    public IDataNode GetDNStub(String Name, String IP, int Port)
    {
        while (true)
        {
            try{
				TimeUnit.SECONDS.sleep(1);
			}catch(InterruptedException ie){
				System.out.println("thread interrrupted " + ie);
			}
            try
            {
                Registry registry = LocateRegistry.getRegistry(IP, Port);
                IDataNode stub = (IDataNode) registry.lookup(Name);
		System.out.println("DataNode found!");
                return stub;
            }
            catch (Exception e)
            {
		System.out.println("Still looking...");
                continue;
            }
        }
    }

    public INameNode GetNNStub(String Name, String IP, int Port)
    {
        while (true)
        {
            try{
				TimeUnit.SECONDS.sleep(1);
			}catch(InterruptedException ie){
				System.out.println("thread interrrupted " + ie);
			}
            try
            {
                Registry registry = LocateRegistry.getRegistry(IP, Port);
                INameNode stub = (INameNode) registry.lookup(Name);
		System.out.println("NameNode found!");
                return stub;
            }
            catch (Exception e)
            {
		System.out.println("Still looking...");
                continue;
            }
        }
    }

    public void PutFile(String Filename) throws Exception
    {
        File file = new File(Filename);
	if (!file.exists())
	{
		System.out.println("Local file not found");
		return;
	}

	if (file.length() == 0)
	{
		System.out.println("Empty file");
		return;
	}

	hdfs.Hdfs.OpenRequest.Builder o1 = hdfs.Hdfs.OpenRequest.newBuilder();
	o1.setFilename(Filename);
	hdfs.Hdfs.OpenResponse o2 = hdfs.Hdfs.OpenResponse.parseFrom(NNStub.openFile(o1.build().toByteArray()));
	if (o2.getExists())
	{
		System.out.println("Remote file already exists");
		return;
	}

	System.out.println("Uploading the file...");
	FileInputStream fis = new FileInputStream(Filename);
	int blocks = (int) Math.ceil(file.length() / 64.0);
	for (int i = 1; i <= blocks; i++)
	{
		hdfs.Hdfs.WriteRequest.Builder w1 = hdfs.Hdfs.WriteRequest.newBuilder();
		w1.setHandle(Filename);
		w1.setBlock(i);
		byte[] buffer = new byte[64];
		int bytes = fis.read(buffer);
		if (bytes < 64)
		{
			buffer = Arrays.copyOf(buffer, bytes);
		}
		w1.setData(ByteString.copyFrom(buffer));
		hdfs.Hdfs.WriteResponse w2 = hdfs.Hdfs.WriteResponse.parseFrom(DNStub.writeBlock(w1.build().toByteArray()));
		if (w2.getError()) {
			System.out.println("Failed to write.");
			return;
		}
	}

	hdfs.Hdfs.CloseRequest.Builder c1 = hdfs.Hdfs.CloseRequest.newBuilder();
	c1.setFilename(Filename);
	c1.setSize(blocks);
	hdfs.Hdfs.CloseResponse c2 = hdfs.Hdfs.CloseResponse.parseFrom(NNStub.closeFile(c1.build().toByteArray()));

	fis.close();
    }

    public void GetFile(String Filename) throws Exception
    {
        File file = new File(Filename);
	if (file.exists())
	{
		System.out.println("Local file already exists");
		return;
	}

	hdfs.Hdfs.OpenRequest.Builder o1 = hdfs.Hdfs.OpenRequest.newBuilder();
	o1.setFilename(Filename);
	hdfs.Hdfs.OpenResponse o2 = hdfs.Hdfs.OpenResponse.parseFrom(NNStub.openFile(o1.build().toByteArray()));
	if (!o2.getExists())
	{
		System.out.println("Remote file not found");
		return;
	}

	System.out.println("Downloading the file...");
	FileOutputStream fos = new FileOutputStream(Filename);
	int blocks = o2.getSize();
	for (int i = 1; i <= blocks; i++)
	{
		hdfs.Hdfs.ReadRequest.Builder r1 = hdfs.Hdfs.ReadRequest.newBuilder();
		r1.setHandle(Filename);
		r1.setBlock(i);
		hdfs.Hdfs.ReadResponse r2 = hdfs.Hdfs.ReadResponse.parseFrom(DNStub.readBlock(r1.build().toByteArray()));
		if (r2.getError()) {
			System.out.println("Failed to read.");
			return;
		}
		fos.write(r2.getData().toByteArray());
	}

	fos.close();
    }

    public void List() throws Exception
    {
	hdfs.Hdfs.ListRequest.Builder l1 = hdfs.Hdfs.ListRequest.newBuilder();
	hdfs.Hdfs.ListResponse l2 = hdfs.Hdfs.ListResponse.parseFrom(NNStub.list(l1.build().toByteArray()));
	System.out.println("List of remote files:");
	for (String s : l2.getFilenameList())
	{
		System.out.println(s);
	}
    }

  public static void main(String[] args) throws Exception
    {
        //To read config file and Connect to NameNode
        //Initialize the Client
        Client Me = null;
         Me = new Client();
         while(Me!=null){
        System.out.println("Welcome to HDFS!!");
        Scanner Scan = new Scanner(System.in);
        while (true)
        {
            //Scanner, prompt and then call the functions according to the command
            System.out.print("$> "); //Prompt
            String Command = Scan.nextLine();
            String[] Split_Commands = Command.split(" ");

            if (Split_Commands[0].equals("help"))
            {
                System.out.println("The following are the Supported Commands");
                System.out.println("1. put filename ## To put a file in HDFS");
                System.out.println("2. get filename ## To get a file in HDFS");
		System.out.println("2. list ## To get the list of files in HDFS");
            }
            else if (Split_Commands[0].equals("put"))
            {
                //Put file into HDFS
                String Filename;
                try
		{
                    Filename = Split_Commands[1];
                    Me.PutFile(Filename);
                }
		catch (ArrayIndexOutOfBoundsException e)
		{
                    System.out.println("Please type 'help' for instructions");
                    continue;
                }
            }
            else if (Split_Commands[0].equals("get"))
            {
                //Get file from HDFS
                String Filename;
                try
		{
                    Filename = Split_Commands[1];
                    Me.GetFile(Filename);
                }
		catch (ArrayIndexOutOfBoundsException e)
		{
                    System.out.println("Please type 'help' for instructions");
                    continue;
                }
            }
            else if (Split_Commands[0].equals("list"))
            {
                //Get list of files in HDFS
                Me.List();
            }
            else
            {
                System.out.println("Please type 'help' for instructions");
            }
        }
    }
    }
    
}

