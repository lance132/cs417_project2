package ds.hdfs;

import java.io.*;
import java.util.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;
import com.google.protobuf.*;

public class DataNode implements IDataNode
{
    public String name;
    public String ip;
    public int port;

    public DataNode() throws Exception
    {
	BufferedReader br = new BufferedReader(new FileReader("dn_config.txt"));
	String line = br.readLine();
	String[] details = line.split(";");
	name = details[0];
	ip = details[1];
	port = Integer.parseInt(details[2]);

	System.setProperty("java.rmi.server.hostname", ip);
	IDataNode stub = (IDataNode) UnicastRemoteObject.exportObject(this, 0);
	Registry registry = java.rmi.registry.LocateRegistry.createRegistry(port);
	registry.rebind(name, stub);
	System.out.println("DataNode connected");
    }

    public byte[] readBlock(byte[] inp) throws Exception
    {
	hdfs.Hdfs.ReadRequest r1 = hdfs.Hdfs.ReadRequest.parseFrom(inp);
	hdfs.Hdfs.ReadResponse.Builder r2 = hdfs.Hdfs.ReadResponse.newBuilder();
	String blockID = r1.getHandle() + "___" + Integer.toString(r1.getBlock());

	try {
		FileInputStream fis = new FileInputStream(blockID);
		byte[] buffer = new byte[64];
		int bytes = fis.read(buffer);
		if (bytes < 64)
		{
			buffer = Arrays.copyOf(buffer, bytes);
		}
		r2.setData(ByteString.copyFrom(buffer));
		r2.setError(false);
	}
	catch (Exception e) {
		r2.setError(true);
	}

	return r2.build().toByteArray();
    }

    public byte[] writeBlock(byte[] inp) throws Exception
    {
	hdfs.Hdfs.WriteRequest w1 = hdfs.Hdfs.WriteRequest.parseFrom(inp);
	hdfs.Hdfs.WriteResponse.Builder w2 = hdfs.Hdfs.WriteResponse.newBuilder();
	String blockID = w1.getHandle() + "___" + Integer.toString(w1.getBlock());

	try {
		FileOutputStream fos = new FileOutputStream(blockID);
		fos.write(w1.getData().toByteArray());
		w2.setError(false);
	}
	catch (Exception e) {
		w2.setError(true);
	}

	return w2.build().toByteArray();
    }

    public static void main(String args[]) throws Exception
    {
        //Define a Datanode Me
	DataNode me = new DataNode();
    }
}
