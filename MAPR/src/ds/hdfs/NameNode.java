package ds.hdfs;

import java.io.*;
import java.util.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;
import com.google.protobuf.*;
import java.util.concurrent.TimeUnit;

public class NameNode implements INameNode
{
	public String name;
	public String ip;
	public int port;

	HashMap<String, Integer> allFiles = new HashMap<String, Integer>();
	
	public NameNode() throws Exception
	{
		BufferedReader br = new BufferedReader(new FileReader("nn_config.txt"));
		String line = br.readLine();
		String[] details = line.split(";");
		name = details[0];
		ip = details[1];
		port = Integer.parseInt(details[2]);

		System.setProperty("java.rmi.server.hostname", ip);
		INameNode stub = (INameNode) UnicastRemoteObject.exportObject(this, 0);
		Registry registry = java.rmi.registry.LocateRegistry.createRegistry(port);
		registry.bind(name, stub);
		System.out.println("NameNode connected");
	}

	public byte[] openFile(byte[] inp) throws Exception
	{
		hdfs.Hdfs.OpenRequest o1 = hdfs.Hdfs.OpenRequest.parseFrom(inp);
		hdfs.Hdfs.OpenResponse.Builder o2 = hdfs.Hdfs.OpenResponse.newBuilder();
		if (allFiles.containsKey(o1.getFilename()))
		{
			o2.setSize(allFiles.get(o1.getFilename()));
			o2.setExists(true);
		}
		else
		{
			o2.setExists(false);
		}

		return o2.build().toByteArray();
	}
	
	public byte[] closeFile(byte[] inp) throws Exception
	{
		hdfs.Hdfs.CloseRequest c1 = hdfs.Hdfs.CloseRequest.parseFrom(inp);
		hdfs.Hdfs.CloseResponse.Builder c2 = hdfs.Hdfs.CloseResponse.newBuilder();
		allFiles.put(c1.getFilename(), c1.getSize());

		return c2.build().toByteArray();
	}

	public byte[] list(byte[] inp) throws Exception
	{
		hdfs.Hdfs.ListRequest l1 = hdfs.Hdfs.ListRequest.parseFrom(inp);
		hdfs.Hdfs.ListResponse.Builder l2 = hdfs.Hdfs.ListResponse.newBuilder();
		allFiles.forEach((k, v) -> l2.addFilename(k));

		return l2.build().toByteArray();
	}

	public static void main(String[] args) throws Exception
	{
		NameNode me = new NameNode();
	}
}
