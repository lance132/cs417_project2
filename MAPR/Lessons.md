# What works

### Put a file into the hdfs
### Get a file from the hdfs
### List files in the hdfs
### Open a file via the NameNode
### Close a file via the NameNode
### Writing blocks into the DataNode
### Reading blocks from the DataNode
### Error handling for nonexistent, empty, and duplicate files
### Handling read and write errors
### All messages sent with google protocol buffers
### Reading from the config files

# What we learned

### We learned how java rmiregistry allows programs to connect with each other for information at a specified port and how this was useful in terms of allowing the Client, DataNode, and NameNode to communicate.

### We learned how to use protobuf to communicate between different programs.

### We learned how to push and pull code using git to keep the project up to date.

### We learned how to write and read blocks from and to files.

### Lastly, we learned that HDFS is highly available, fault tolerant, and has high reliability.

# SEE README.md for compiling and running the code